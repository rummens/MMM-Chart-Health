'use strict';
/* global Module */

/* Magic Mirror
 * Module: MMM-Chart-Health
 *
 * By Marcel Rummens https://gitlab.com/rummens/MMM-Chart-Health/
 * MIT Licensed.
 */

Module.register("MMM-Chart-Health", {

    jsonData: null,

    defaults: {
        width : 600,
        height : 800,
        blank_height : 80,
        url : "http://smart-home.fritz.box:9000/health/data?limit=7",
        key_steps: "steps",
        key_sleep: "sleep_minutes",
        updateInterval: 15000,
        arrayName: null
    },

    getScripts: function() {
		return ["https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.min.js"];
	},

	start: function() {
        this.config = Object.assign({}, this.defaults, this.config);
		Log.info("Starting module: " + this.name);
		this.getJson();
		this.scheduleUpdate();
	},

	scheduleUpdate: function () {
		var self = this;
		setInterval(function () {
			self.getJson();
		}, this.config.updateInterval);
	},

	// Request node_helper to get json from url
	getJson: function () {
		this.sendSocketNotification("MMM-Chart_HEALTH_GET_JSON", this.config.url);
	},

	socketNotificationReceived: function (notification, payload) {
		if (notification === "MMM-Chart_HEALTH_JSON_RESULT") {
			// Only continue if the notification came from the request we made
			// This way we can load the module more than once
			if (payload.url === this.config.url)
			{
				this.jsonData = payload.data;
				this.updateDom(500);
			}
		}
	},

	getDom: function() {
        // Create wrapper element
        const wrapperEl = document.createElement("div");
        wrapperEl.setAttribute("style", "position: relative; display: inline-block;");

        if (!this.jsonData) {
			wrapperEl.innerHTML = "Awaiting json data...";
			return wrapperEl;
		}

		var items = [];
		if (this.config.arrayName) {
			items = this.jsonData[this.config.arrayName];
		}
		else {
			items = this.jsonData;
		}

		// Check if items is of type array
		if (!(items instanceof Array)) {
			wrapperEl.innerHTML = "Json data is not of type array! " +
				"Maybe the config arrayName is not used and should be, or is configured wrong";
			return wrapperEl;
		}

		var chartConfig_steps = this.parse_data(items, this.config.key_steps, '#f32144', "Schritte")
		var chartConfig_sleep = this.parse_data(items, this.config.key_sleep, '#2196f3', "Schlaf")

        // display error
		if (typeof chartConfig_steps === 'string' || chartConfig_steps instanceof String) {
		    wrapperEl.innerHTML = chartConfig_steps;
			return wrapperEl;
		}
		if (typeof chartConfig_sleep === 'string' || chartConfig_sleep instanceof String) {
		    wrapperEl.innerHTML = chartConfig_sleep;
			return wrapperEl;
		}

        // Create chart canvas
        const chartEl_sleep  = document.createElement("canvas");
        const chartEl_steps  = document.createElement("canvas");
        const blank  = document.createElement("canvas");

        // Init chart.js
        this.chart_steps = new Chart(chartEl_steps.getContext("2d"), chartConfig_steps);
        this.chart_sleep = new Chart(chartEl_sleep.getContext("2d"), chartConfig_sleep);

	    // Set the size
	    chartEl_steps.width  = this.config.width;
	    chartEl_steps.height = this.config.height/2;
	    chartEl_steps.setAttribute("style", "display: block;");

	    chartEl_sleep.width  = this.config.width;
	    chartEl_sleep.height = this.config.height/2;
        chartEl_sleep.setAttribute("style", "display: block;");

        blank.height = this.config.blank_height;

        // Append chart
        wrapperEl.appendChild(chartEl_steps);
        wrapperEl.appendChild(blank);
        wrapperEl.appendChild(chartEl_sleep);

		return wrapperEl;
	},

	parse_data: function(jsonArray, object_key, color, label) {

	    var labels = []
	    var data = []
        var loop_number = 0

	    for (var entry in jsonArray) {
	        var obj = jsonArray[entry]

	        if (!(object_key in obj)) {
                return "Can`t find key " +  object_key + " in object " + JSON.stringify(obj)
	        } else {
                labels.push(this.get_day_from_date(obj["ingested"]))
                data.push(obj[object_key])
	        }
	    }

	    return {
            type: 'line',
            data: {
        labels: labels,

        datasets: [{
            data: data, // Specify the data values array
            fill: false,
            label: label,
            borderColor: color, // Add custom color border (Line)
            backgroundColor: color, // Add custom color background (Points and Fill)
            borderWidth: 3, // Specify bar border width
            pointRadius: 5
        }]},
            options: {
                maintainAspectRatio: true,
                legend: {
                    display: true,
                    labels: {
                        fontSize: 20,
                        fontStyle: "bold",
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontSize: 20,
                            fontStyle: "bold",
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            fontSize: 20,
                            fontStyle: "bold",
                        }
                    }]
                }
            }
        }
	},

	get_day_from_date: function (date_string) {
        var date = new Date(date_string)
        var days = ['Sontag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'];
        return days[date.getDay()];
	}
});
